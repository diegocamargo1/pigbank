import React,{ useEffect, useState } from 'react'
import { create } from "../../service/api"

import Header from '../../components/Header/Header'
import Button from '../../components/Button/Button'
import Title from '../../components/Title/Title'

import Footer from '../../components/Footer/Footer'
import Card from '../../components/Card/Card'

function CotaçãoDeMoedas() {

    const [data, setData] = useState()

    useEffect(()=>{
        create.get("/")
        .then((res) => {
            setData(res.data)
            console.log(data)
        })
    }, [])

    return (
        <section>
            <Header/>
            <Title text="Cotação Moedas"/>
            <Button text="Cotação Moedas" className="pink"/>
            <Footer/>
        </section>
    )
}

export default CotaçãoDeMoedas
