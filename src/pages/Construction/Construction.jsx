import React from 'react'
import Button from '../../components/Button/Button'
import Header from '../../components/Header/Header'
import Title from '../../components/Title/Title'
import Footer from '../../components/Footer/Footer'
import { ReactComponent as ImageContruction} from '../../assets/construction.svg'

import "./Construction.css"

function Construction() {
    return (
        <>
        <Header/>
        <section className="sectionConstruction">
            <Title text="Em contrução"/>
            <ImageContruction/>
            <p>Parece que essa página ainda não foi implementada... Tente novamente mais tarde!</p>
            <Button text="Voltar" className="pink"/>
        </section>
        <Footer/>
        </>
    )
}

export default Construction
