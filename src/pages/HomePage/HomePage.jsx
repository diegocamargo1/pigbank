import React from 'react'
import Header from '../../components/Header/Header'
import Button from '../../components/Button/Button'
import Title from '../../components/Title/Title'
import { ReactComponent as ImageFinances } from '../../assets/finances.svg'
import { ReactComponent as ImageIncome } from '../../assets/income.svg'

import "./HomePage.css"
import Footer from '../../components/Footer/Footer'

function HomePage() {
    return (
        <section>
            <Header/>
            <section className="sectionContentFinances">
                <ImageFinances/>
                <section className="sectionText">
                    <h1>A mais nova altenativa de banco digital chegou!</h1>
                    <p>Feito para caber no seu bolso e otimizar seu tempo. O PigBank veio pra ficar</p>
                    <Button text="Abra a sua conta" className="pink"/>
                </section>
            </section>
            <Title text="Fique por dentro!"/>
            <section className="sectionContentIncome">
                <section>
                <p>Ao contrário do ditado popular, por aqui, quem se mistura com porco não come farelo! Conheça nossa plataforma exclusivamente dedicada para ampliar o seu patrimônio.</p>
                <ImageIncome/>
                </section>
                <Button text="Cotação Moedas" className="pink"/>
            </section>
            
            <Footer/>
            
        </section>
    )
}

export default HomePage
