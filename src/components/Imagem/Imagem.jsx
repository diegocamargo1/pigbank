import React from 'react'
import "./Imagem.css"

function Imagem({url,alt}) {
    return (
        <img src={url} alt={alt} />
    )
}

export default Imagem
