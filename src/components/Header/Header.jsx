import React from 'react'
import Button from '../Button/Button'
import "./Header.css"

function Header() {
    return (
        <>
            <header>
                <nav>
                    <h1>Pigbank</h1>
                    <ul>
                        <li>
                            <Button text="Cotação Moedas" className="pink"/>
                        </li>
                        <li>
                            <Button text="Acessar" className="white"/>
                        </li>
                    </ul>
                </nav>
            </header>
        </>
    )
}

export default Header
