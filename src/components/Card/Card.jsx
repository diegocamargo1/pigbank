import React from 'react'

import"./Card.css"

function Card({info}) {
    return (
        <section className="sectionCard">
            <section className="cardName">
                <p>Dólar Turismo</p>
                <p className="sigle">USD</p>
            </section>
            <div></div>
            <section className="cardValues">
                <p>Maxima: R$ 3,8906</p>
                <p>Mínima: R$ 3,8596</p>
                <p className="cardDescription">Atualizado em 22/01/2021 às 17:27:00</p>
            </section>
            {}
        </section>
    )
}

export default Card
