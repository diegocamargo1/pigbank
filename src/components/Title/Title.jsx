import React from 'react'
import "./Title.css"

function Title({text}) {
    return (
        <section className="title">
            <hr/>
            <h1>{text}</h1>
        </section>
    )
}

export default Title
