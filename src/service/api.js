import axios from "axios"

const create = axios.create({
    baseURL: "https://economia.awesomeapi.com.br/json/all"
})

export{ create }