import React from "react"
import { BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom"
import Construction from "../pages/Construction/Construction"
import CotaçãoDeMoedas from "../pages/CotaçãoDeMoedas/CotaçãoDeMoedas"
import HomePage from "../pages/HomePage/HomePage"


function Routes() {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <HomePage/>
                </Route>
                <Route exact path="/CotaçãoMoedas">
                    <CotaçãoDeMoedas/>
                </Route>
                <Route exact path="/Construction">
                    <Construction/>
                </Route>
                <Route exact path="/Register">
                    <Redirect to="/Construction"/>
                </Route>
                <Route exact path="/Login">
                    <Redirect to="/Construction"/>
                </Route>
            </Switch>
        </Router>
    )
}

export default Routes